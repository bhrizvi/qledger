The Project is deployed on Provisioned ECR(Elastic Container Registery) and tasks run on ECS in AWS account in us-east-1 (N.Virgina) region.

The project is deployed in GitLab as:
Git Repostiory: https://gitlab.com/bhrizvi/QLedger/
Git Pipelines: https://gitlab.com/bhrizvi/QLedger/-/pipelines

To deploy to an AWS account folowing secrets are required to be added in Git Settings
AWS_ACCESS_KEY_ID
AWS_DEFAULT_REGION
AWS_SECRET_ACCESS_KEY

There are 4 stages of Pipeline for this project
stages:
  - build (manual)
  - deploy (manual)

As soon as the project is pushed to  Gitlab repository the pipeline can be started to deploy the resources to ECR and ECS. Both stages have to be triggered manually